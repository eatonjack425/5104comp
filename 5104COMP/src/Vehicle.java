import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
/**
 * Vehicle is the abstract base type to represent either a truck or tanker
 * @author pablo @author tristan
 *
 */
public abstract class Vehicle implements Serializable{
public void setAssigned(List<WorkSchedule> assigned) {
		this.assigned = assigned;
	}
protected String make ;
protected String model;
protected int weight;
protected String regNo;
protected List<WorkSchedule> assigned = new ArrayList<WorkSchedule>();
/**
 * Constructor
 * @param make
 * @param model
 * @param weight
 * @param regNo
 */
public Vehicle (String make, String model, int weight, String regNo) {
	this.make = make;
	this.model = model;
	this.weight = weight;
	this.regNo = regNo;
}
/**
 * Checks the availability of the vehicle
 * @param aDate input parameter to check the data that  the driver is available
 * @return whether the driver is available or not in a boolean 
 */
public boolean isAvaliable(Date sDate, Date eDate) {

	Date dayBefore = sDate;
	
	Calendar calendar = Calendar.getInstance();
    dayBefore = calendar.getTime();

    calendar.add(Calendar.DAY_OF_YEAR, -1);
    for(WorkSchedule currWS : assigned) {
	if(eDate.after(currWS.getEndDate()) || dayBefore.before(currWS.getStartDate())) {
		return true;
	}else {
		return false;
	}
    }
	return true;
}

/**
 * Assigns a schedule to the vehicle
 * @param newSchedule
 */
public  void setSchedule(WorkSchedule newSchedule) {
	this.assigned.add(newSchedule); 
}

public List<WorkSchedule> getAssigned() {
	return assigned ;
}

public String getMake() {
	return make;
}
public void setMake(String make) {
	this.make = make;
}
public String getModel() {
	return model;
}
public void setModel(String model) {
	this.model = model;
}
public int getWeight() {
	return weight;
}
public void setWeight(int weight) {
	this.weight = weight;
}
public String getRegNo() {
	return regNo;
}
public void setRegNo(String regNo) {
	this.regNo = regNo;
}
}
