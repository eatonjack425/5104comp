import java.io.Serializable;
import java.util.Date;

/**
 * The work schedule class represents a scheduled job for a set client from end to start date
 * @author pablo
 *
 */
public class WorkSchedule implements Serializable {
	
private String client ;
private Date startDate ;
private Date endDate;
public String getState() {
	return state;
}

public void setState(String state) {
	this.state = state;
}

private String state;
/**
 * Constructor
 * @param client
 * @param startDate
 * @param endDate
 */
public WorkSchedule(String client, Date startDate, Date endDate) {
	this.client = client;
	this.startDate = startDate;
	this.endDate = endDate;
}

public String getClient() {
	return client;
}

public void setClient(String client) {
	this.client = client;
}

public Date getStartDate() {
	return startDate;
}

public void setStartDate(Date startDate) {
	this.startDate = startDate;
}

public Date getEndDate() {
	return endDate;
}

public void setEndDate(Date endDate) {
	this.endDate = endDate;
}

}
