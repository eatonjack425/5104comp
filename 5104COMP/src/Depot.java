import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Depot implements Serializable {
	
		private String depotName;
		private List<Driver> drivers = new ArrayList<Driver>();
		private List<Vehicle> vehicles = new ArrayList<Vehicle>();
		private List<WorkSchedule> workSchedules = new ArrayList<WorkSchedule>();
		/**
		 *Constructor
		 *@param List of Drivers
		 *@param List of Vehicles
		 *@param List of Work Schedules
		 *@author Jack Eaton
		 */
public Depot(String depot)
{
	this.depotName = depot;
}

public String getDepot() {
	return depotName;
}
public void setDepot(String depot) {
	this.depotName = depot;
}
public List<Driver> getDrivers() {
	return drivers;
}

public void setDrivers(List<Driver> drivers) {
	this.drivers = drivers;
}
public List<Vehicle> getVehicles() {
	return vehicles;
}
public void setVehicles(List<Vehicle> vehicles) {
	this.vehicles = vehicles;
}
public List<WorkSchedule> getWorkSchedules() {
	return workSchedules;
}
public void setWorkSchedules(List<WorkSchedule> workSchedules) {
	this.workSchedules = workSchedules;
}
}


