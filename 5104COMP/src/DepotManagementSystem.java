import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class DepotManagementSystem {
	private static String absolutePath = "";
	private final static Scanner input = new Scanner(System.in);
	private final static String filePath = "dataFile.ser";
	private static String inputControl = null;
	private static ArrayList<Depot> depotList = new ArrayList<Depot>();
	private static Depot currentDepot = new Depot("");
	private static Driver currentDriver = new Driver("","");
	
	/**
	 * Main collection of methods for the System
	 * @author Jack Eaton
	 */
	public DepotManagementSystem() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * User Login & Error Handling
	 * @author Jack Eaton
	 */
	public void logOn() {
		boolean flag = true;
		while (flag)
		{
		try {
		deserialize();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		Driver currentDriver = loginMenu();
		try
		{
		flag = mainMenu();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (!flag)
			{
			System.out.println("Exit Programs");
			try {
				serialize();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			System.exit(1);
		}
		}
	}
	}

	
	public static boolean containsOnlyChar(String s) {
		return s != null && s.chars().allMatch(Character::isLetter);
	}
	
	/**
	 * User Login and Validation of User Input
	 * @return Current Driver
	 * @author Jack Eaton
	 */
	public Driver loginMenu()
{
	currentDriver = new Driver("","");
	currentDepot = new Depot("");
	
	Driver currentUser = null;
	char RoleFlag = 'm';
	String inputUsername = "";
	String inputPassword = "";
	boolean menuFlag = false;

	if (depotList.size() < 1)
	{
		generateTempData();
	}
	
	while (!menuFlag) {
		System.out.println(
				" ---- Depot System ---- \n Please Sign-in to your account by entering the following credentials.");

		System.out.println("Please enter your username: ");
		inputUsername = input.nextLine();
		System.out.println("Please enter the password: ");
		inputPassword = input.nextLine();
		
		for(Depot depot : depotList)
		{
			for(Driver driver : depot.getDrivers())
			{
				//System.out.println(inputUsername + " " + );
				if (inputUsername.equalsIgnoreCase(driver.getUsername()) && inputPassword.equalsIgnoreCase(driver.getPassword()))
				{
					currentDriver = driver;
					currentDepot = depot;
					menuFlag = true;
					break;
				}
			}
		}
		if (!menuFlag)
		{
			System.out.println("\n"+"The username or password you have entered is invalid, please try again"+"\n");

		}
		//menuFlag = true; //Once users exist, validation will be present which will change this value to true if correct
	}
	return currentUser;
}

	/**
	 * Allocates Role Depending on Instance of Class and outputs appropriate Menu 
	 * 
	 * @author Jack Eaton
	 * @return 
	 */
public boolean mainMenu() throws FileNotFoundException, IOException
{
	boolean loginFlag = false;
	boolean menuFlag = false;
	char RoleFlag;
	
	if (currentDriver instanceof Manager)
	{
		 RoleFlag = 'm';
	}
	else
		RoleFlag = 'd';
	
	updateSchedule();
	
	File tempDir = new File(filePath);
	absolutePath = tempDir.getAbsolutePath();
	boolean fileExists = tempDir.exists();	
		
	while (!menuFlag) {
		if (RoleFlag == 'm') {
			System.out.println(
					"\n --- Main Menu (Manager View) ---\n -- Input one of the numeric options -- \n1 - Create Work Schedule \n2 - View Work Schedule \n3 - Reassign Vehicle to different Depot \nQ - Save and Sign-Out \nX - Save and Close");

			inputControl = input.nextLine();

			switch (inputControl) {
			case "1": {					
				
				Date startDate = new Date();
				Date endDate = new Date();
				String inputClient = "";
				
				while (!menuFlag)
				{
				System.out.println("Enter a client name e.g Joe");
				inputClient = input.nextLine();
				if (containsOnlyChar(inputClient))
				{
					menuFlag = true;
				}
				else
					System.out.println("The Client Name you have entered contains an invalid character, only Alphabetic Characters are allowed. Please try again");
				}
				menuFlag = false;
				
				boolean flag = false;
				Date todayDate = new Date();
				while(!flag) {
				System.out.println("Enter a start date in the format: dd/mm/yyyy");
				String inputStart = input.nextLine();
				startDate = parseDate(inputStart);
				
				
				
				if(startDate == null) {
					flag = false;
					System.out.println("Dates must be in the format: dd/mm/yyyy"+"\n");
				
				}else if (startDate != null && startDate.after(todayDate)){
				
					flag = true;
				}
				
				else {
					System.out.println("\n"+"Start date must be after the current date");
					flag = false;
				}
				}
				
				while(flag) {
				System.out.println("Enter an end date in the format: dd/mm/yyyy");
				String inputEnd = input.nextLine();
				endDate = parseDate(inputEnd);
				
				if(endDate == null) {
					flag = true;
					System.out.println("Dates must be in the format: dd/mm/yyyy"+"\n");
					
				}else if (endDate != null && endDate.after(startDate)){
				
					flag = false;
				}
				
				else {
					System.out.println("\n"+"End date must be after the start date");
					flag = true;
				}
				}
				createSchedule(inputClient, startDate, endDate);
				
				break;
			}
			case "2": {

				viewSchedule();
				break;
			}
			case "3": {
				reassignVehicle();
				break;
			}
			case "q": {
				menuFlag = true;
				loginFlag = true;
				break;
			}
			case "x": {
				menuFlag = true;
				loginFlag = false;
				break;
			}
			default: {
				System.out.println("Please input one of the numeric options (e.g. 1)");
				break;
			}
			}
		} else if (RoleFlag == 'd') {
			System.out.println("\n --- Main Menu (Driver View) ---\n -- Input one of the numeric options -- \n1 - View Driver's Work Schedule \nQ - Save and Sign-Out \nX - Save and Close");
			inputControl = input.nextLine();
			switch (inputControl) {
			case "1": {
				viewSchedule();
				break;
			}

			case "q": {
				menuFlag = true;
				loginFlag = true;
				break;

			}
			case "x": {
				menuFlag = true;
				loginFlag = false;
				break;
			}


			default: {
				System.out.println("Please input one of the numeric options (e.g. 1)");
				break;
			}
			}
		}
	}
	return loginFlag;
}
public static void generateTempData()
{
	try {
	//temporary code to generate data for file
	Depot depot = new Depot("Manchester Depot");
	List<Driver> tempDrivers = new ArrayList<Driver>();
	Driver tempDriver = new Driver("userman","password");
	Driver tempManager = new Manager("Sorren","_Sorren");
	tempDrivers.add(tempDriver);
	tempDriver = new Driver("user1man","password");
	tempDrivers.add(tempDriver);
	tempDriver = new Driver("user2man","password");
	tempDrivers.add(tempDriver);
	tempDriver = new Driver("user3man","password");
	tempDrivers.add(tempDriver);
	tempDrivers.add(tempManager);
	depot.setDrivers(tempDrivers);
	List<Vehicle> tempVehicles = new ArrayList<Vehicle>();
	tempVehicles.add(new Tanker("Fond", "ExtraLarge", 420, "B8Q 54UC3", 100, "Sauce"));
	tempVehicles.add(new Truck("Valva", "BigYoke", 420, "81G Y0K3", 10));
	depot.setVehicles(tempVehicles);
	depotList.add(depot);
	
	depot = new Depot("Liverpool Depot");
	tempDrivers = new ArrayList<Driver>();
	tempDriver = new Driver("userliverpool","password");
	tempManager = new Manager("Glyn","_Glyn");
	tempDrivers.add(tempDriver);
	tempDriver = new Driver("user1liverpool","password");
	tempDrivers.add(tempDriver);
	tempDriver = new Driver("user2liverpool","password");
	tempDrivers.add(tempDriver);
	tempDriver = new Driver("user3liverpool","password");
	tempDrivers.add(tempDriver);
	tempDrivers.add(tempManager);
	depot.setDrivers(tempDrivers);
	tempVehicles = new ArrayList<Vehicle>();
	tempVehicles.add(new Tanker("Vauxaul", "Highlander", 420, "54UC3 B8Q", 100, "Sauce"));
	tempVehicles.add(new Truck("Toyota", "Transit", 420, "Y0K3 81G", 10));
	depot.setVehicles(tempVehicles);
	depotList.add(depot);
	
	depot = new Depot("Leeds Depot");
	tempDrivers = new ArrayList<Driver>();
	tempDriver = new Driver("userbirmingham","password");
	tempManager = new Manager("Kirsty","_Kirsty");
	tempDrivers.add(tempDriver);
	tempDriver = new Driver("user1birmingham","password");
	tempDrivers.add(tempDriver);
	tempDriver = new Driver("user2birmingham","password");
	tempDrivers.add(tempDriver);
	tempDriver = new Driver("user3birmingham","password");
	tempDrivers.add(tempDriver);
	tempDrivers.add(tempManager);
	depot.setDrivers(tempDrivers);
	tempVehicles = new ArrayList<Vehicle>();
	tempVehicles.add(new Tanker("Nissan", "Transport", 420, "54UC3 B8Q", 100, "Sauce"));
	tempVehicles.add(new Truck("Toyota", "Max", 420, "Y0K3 81G", 10));
	depot.setVehicles(tempVehicles);
	depotList.add(depot);
	
	System.out.println("Temp Data Entered");
		serialize();
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

/**
 * User Login and Validation of User Input
 * @param String of Date (dd/MM/yyyy)
 * @return Date
 * @author Jack Eaton
 */
public Date parseDate(String tempDateString)
{
	Date TempDate = null;
	try {
		TempDate = new SimpleDateFormat("dd/MM/yyyy").parse(tempDateString);
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		System.out.println(e);
	}
	finally 
	{
	return TempDate;
	}
}

/**
 * Read File from File to DepotList
 * @param .ser File
 * @return depotList
 * @author Jack Eaton
 */
@SuppressWarnings("unchecked")
private static void deserialize() throws FileNotFoundException, IOException {
	File tempDir = new File(filePath);
	absolutePath = tempDir.getAbsolutePath();
	try {
	ObjectInputStream ois = new ObjectInputStream(new FileInputStream(absolutePath));
	try {
		// NOTE : Erasure Warning !
		depotList = (ArrayList<Depot>)ois.readObject();
	}
	catch (Exception e) {
		System.out.println(e.getMessage());
	}
	finally
	{
		try {
			ois.close();
			System.out.println("File Closed Successfully");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	}
	catch (FileNotFoundException filenotfound)
	{
		System.out.println(filenotfound.getMessage());
		
	}
	catch (Exception e)
	{
		System.out.println(e.getMessage());
	}
}

/**
 * Write DepotList to File
 * @param depotList
 * @return .ser File
 * @author Jack Eaton
 */
private static void serialize() throws FileNotFoundException, IOException {
	File tempDir = new File(filePath);
	absolutePath = tempDir.getAbsolutePath();
	depotList.add(currentDepot);
	ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(absolutePath));
	try {
        oos.writeObject(depotList);
        System.out.println("Object written to File");
	}
	catch (Exception e) {
		e.printStackTrace();
	}
	finally
	{
		 try {
			oos.close();
			System.out.println("File Closed Successfully");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

/**
 * Create schedule makes a schedule for designated client with start and end date 
 * then you choose which driver and vehicle to assign to the 
 *  @param clientName this is the client that the work schedule is going to be assigned to
 *  @param startDate this is the start date for the new schedule
 *  @param endDate this the ending date for the new schedule 
 *  @param currentUser is the currently signed in user
 *  @author Pablo
 */
private static void createSchedule(String clientName, Date startDate, Date endDate) {

	Vehicle cVehicle = null ;
	Driver cDriver = null ;
	if(currentDriver instanceof Manager) {
		WorkSchedule newSchedule = new WorkSchedule(clientName,startDate,endDate);
		for (Driver currD : currentDepot.getDrivers()) {
			if (currD.isAvaliable(startDate, endDate)) {
			System.out.println("Name: "+currD.getUsername());
			}
		}
		boolean flag = false; 
		boolean flag1 = true;
		while(!flag) {
		System.out.println("Choose which driver you wish to assign to the work schedule (input as username appears)");
		String selectedUser = input.nextLine();
			for (Driver currD : currentDepot.getDrivers()) {
				if(currD.username.equalsIgnoreCase(selectedUser) && currD.isAvaliable(startDate, endDate)) {
					cDriver = currD;
					flag = true;
				
		}
			}
			if (!flag)
			{
				System.out.println("Driver was not in the list");
			}
		}
		flag = false;
		while(!flag) {
		
		System.out.println("What type of Vehicle would you like to select? (input numberical value that represents vehicle type) \n1 - Truck \n2 - Tanker\n ");
		String typeChoice = input.nextLine();
		if (typeChoice.equals("1"))
		{
			for (Vehicle currV : currentDepot.getVehicles()) {
				if(currV.isAvaliable(startDate, endDate) && currV instanceof Truck) {
				System.out.println("reg num: "+currV.getRegNo());
				flag = true;
			}
		}
			while(flag1) {
				System.out.println("Choose which vehicle you wish to the work schedule (input as reg no appears)");
				String selectedVehicle = input.nextLine();
					for (Vehicle currV : currentDepot.getVehicles()) {
						if(currV.regNo.equalsIgnoreCase(selectedVehicle) && currV.isAvaliable(startDate, endDate) && currV instanceof Truck) {
							cVehicle = currV;
							flag1 = false;
						}
				}
				if (!flag1) {
					cVehicle.setSchedule(newSchedule);
					cDriver.setSchedule(newSchedule);
					currentDepot.getWorkSchedules().add(newSchedule);
					System.out.println("Data Successfully Added");
					flag = true;
				}
				else
					System.out.println("Vehicle was not in the list");
				
				}
		}
		else if (typeChoice.equals("2"))
		{
			for (Vehicle currV : currentDepot.getVehicles()) {
				if(currV.isAvaliable(startDate, endDate) && currV instanceof Tanker) {
				System.out.println("reg num: "+currV.getRegNo());
				flag1 = true;
			}
				
				
			}
			while(flag1) {
				System.out.println("Choose which vehicle you wish to the work schedule (input as reg no appears)");
				String selectedVehicle = input.nextLine();
					for (Vehicle currV : currentDepot.getVehicles()) {
						if(currV.regNo.equalsIgnoreCase(selectedVehicle) && currV.isAvaliable(startDate, endDate) && currV instanceof Tanker) {
							cVehicle = currV;
							flag1 = false;
						
						}
				}
				if (!flag1) {
					cVehicle.setSchedule(newSchedule);
					cDriver.setSchedule(newSchedule);
					currentDepot.getWorkSchedules().add(newSchedule);
					flag = true;
				}
				else
					System.out.println("Vehicle was not in the list");
				
				}
		}
			else
				System.out.println("Invalid Input, please enter the numeric value that represents the vehicle type and try again");
		
		}
		
	}
	try {
		serialize();
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}


/**
 * updates the state of all the schedules for the current depot 
 */
public static void updateSchedule() {
	Calendar calendar = Calendar.getInstance();
    Date today = calendar.getTime();
    calendar.add(Calendar.DAY_OF_YEAR, -1);
    Date datedays = calendar.getTime();
	
	for(WorkSchedule currWS : currentDepot.getWorkSchedules()) {
		if(currWS.getStartDate().after(datedays)) {
			currWS.setState("Pending");
		}
		else if (currWS.getStartDate().before(today) && currWS.getEndDate().after(today) ) {
			currWS.setState("Active");
		}
		else if (currWS.getEndDate().after(today)) {
			currWS.setState("Archived");
		}
	}
}

/**
 * shows all the work schedules if instance of manager or shows drivers work schedule
 * @author Tristan 
 */
/**
 * shows all the work schedules if intance of manager or shows drivers work schedule
 * @author Tristan 
 */
public static void viewSchedule() {
	boolean flag = true;
	
	if(currentDriver instanceof Manager) {
		
		while (flag)
		{
			System.out.println("Please select filter for work schedule.\n1 - View All \n2 - Filter by User (username required) \n3 - Filter by Vehicle (reg no required)");
			String wsfilter = input.nextLine();
			
			if (wsfilter.equals("1"))
			{
				System.out.println("Work Schedules: ");
				for(WorkSchedule schedule : currentDepot.getWorkSchedules()) {
					System.out.println("Work schedule for " + schedule.getClient());
					System.out.println("Start date " + schedule.getStartDate());
					System.out.println("End date " + schedule.getEndDate());
				}
				flag = false;
			}
			else if (wsfilter.equals("2"))
			{
				for (Driver d : currentDepot.getDrivers())
				{
					System.out.println("Username: " + d.getUsername());
				}
				flag = false;
				while(!flag) {
				System.out.println("Please enter the username you wish to search by");
				wsfilter = input.nextLine();
				for (Driver d : currentDepot.getDrivers())
				{
					if (d.username.equals(wsfilter))
					{
						for(WorkSchedule schedule : d.getAssigned()) {
							System.out.println("Work schedule for " + schedule.getClient());
							System.out.println("Start date " + schedule.getStartDate());
							System.out.println("End date " + schedule.getEndDate());
					}						
				}
				}
				flag = true;
			}
				flag = false;
			}
			else if (wsfilter.equals("3"))
			{
				for (Vehicle v : currentDepot.getVehicles())
				{
					System.out.println("Reg No: " + v.getRegNo());
				}				
				flag = false;
				while(!flag) {
				System.out.println("Please enter the reg no you wish to search by");
				wsfilter = input.nextLine();
				for (Vehicle v : currentDepot.getVehicles())
				{
					if (v.regNo.equals(wsfilter))
					{
						for(WorkSchedule schedule : v.getAssigned()) {
							System.out.println("Work schedule for " + schedule.getClient());
							System.out.println("Start date " + schedule.getStartDate());
							System.out.println("End date " + schedule.getEndDate());
					}						
				}
				}
				flag = true;
			}
				flag = false;
			}
			
			else
				System.out.println("Invalid Input, please enter the numeric value that represents the filter type and try again");
		}
		
		
		flag = false;
		
	}else {
		/*
		System.out.println(currentDriver.getAssigned().size());
		 for (WorkSchedule currWS : currentDriver.getAssigned()) {
		System.out.println("Current Work Schedule Client: " + currWS .getClient());
		System.out.println("Current Work Schedule Start Date: " + currWS .getStartDate());
		System.out.println("Current Work Schedule End Date: " + currWS .getEndDate());*/
		
		String usernameSelect = currentDriver.getUsername();
		for (Driver d : currentDepot.getDrivers())
		{
			if (d.username.equals(usernameSelect))
			{
				for(WorkSchedule schedule : d.getAssigned()) {
					System.out.println("Work schedule for " + schedule.getClient());
					System.out.println("Start date " + schedule.getStartDate());
					System.out.println("End date " + schedule.getEndDate());
			}						
		}
		
		flag = true;
	}
		 }
	}
	



/**
 * method that will remove chosen vehicle from current depot and reassign to a selected depot
 * @author Tristan
 */

public static void reassignVehicle() {
			
		if(currentDriver instanceof Manager) {
			
			Vehicle currVehicle = null ;
			Depot currDepot = null;
			Date sDate = new Date();
			Date eDate = new Date();
			
			Calendar calendar = Calendar.getInstance();
		    eDate = calendar.getTime();

		    calendar.add(Calendar.WEEK_OF_YEAR, + 1);
		    
				boolean flag = true;
				ArrayList<Vehicle> selVehicleList = new ArrayList<Vehicle>();
				while (flag)
				{
				System.out.println("What type of Vehicle would you like to select?\n1 - Truck \n2 - Tanker\n");
				String typeChoice = input.nextLine();
				if (typeChoice.equals("1"))
				{
					for (Vehicle currV : currentDepot.getVehicles()) {
						if(currV.isAvaliable(sDate, eDate) && currV instanceof Truck) {
							selVehicleList.add(currV);
						System.out.println("reg num: "+currV.getRegNo());
						flag = false;
					}
				}
				}
				else if (typeChoice.equals("2"))
					{
						for (Vehicle currV : currentDepot.getVehicles()) {
							if(currV.isAvaliable(sDate, eDate) && currV instanceof Tanker) {
							selVehicleList.add(currV);
							System.out.println("reg num: "+currV.getRegNo());
							flag = false;
						}
					}
					}
				else
					System.out.println("Invalid Input, please enter the numeric value that represents the vehicle type and try again");
				}
				while(!flag) {
				System.out.println("Choose which vehicle you wish to the reassign (input as vehicle reg appears)");
				String selectedVehicle = input.nextLine();
				
					for (Vehicle vehicle : selVehicleList) {
						if(vehicle.regNo.equalsIgnoreCase(selectedVehicle) && vehicle.isAvaliable(sDate, eDate)) {
							currVehicle = vehicle;
							flag = true;
						}
						}
					if (!flag)
						System.out.println("Vehicle not on the list or has a job in the next week");
					else
						currentDepot.getVehicles().remove(currVehicle);
				}
				
				for(Depot cdepot : depotList) {
					if (!cdepot.getDepot().isEmpty())
					System.out.println("Depot: "+ cdepot.getDepot()); 
				}
				
				while(flag) {
					System.out.println("Which Depot do you want to move this vehicle to? (input as name appears)");
					String selectedDepot = input.nextLine();
					
					for (Depot cdepot : depotList) {
						if(cdepot.getDepot().equalsIgnoreCase(selectedDepot)) {
							currDepot = cdepot;
							flag = false;
						}
						}
					if (flag)
						System.out.println("Depot not on the list");
					else
					{
						currDepot.getVehicles().add(currVehicle);
						System.out.println("Vehicle added");
					}
						
				}
					
		}else {
			System.out.println("You must be a manager to do this");
		}
		try {
			serialize();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

