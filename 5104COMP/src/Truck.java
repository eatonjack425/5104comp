/**
 * Truck is a type of vehicle for solid object transport
 * @author pablo
 *
 */
public class Truck extends Vehicle{
	public int cargoCapacity;
	/**
	 * Constructor
	 * @param make
	 * @param model
	 * @param weight
	 * @param regNo
	 * @param cargoCapacity
	 */
	public Truck(String make, String model, int weight, String regNo, int cargoCapacity) {
		super(make, model, weight, regNo);
		this.cargoCapacity = cargoCapacity;
		// TODO Auto-generated constructor stub
	}
	public int getCargoCapacity() {
		return cargoCapacity;
	}
	public void setCargoCapacity(int cargoCapacity) {
		this.cargoCapacity = cargoCapacity;
	}

}
