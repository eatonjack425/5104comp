
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
* Driver class represents the base user
* 
* @author  pablo prince @author tristan
*/
public class Driver implements Serializable{
protected String username = "";
protected String password = "";
protected List<WorkSchedule> assigned = new ArrayList<WorkSchedule>();
/**
/**
 * Constructor
 * @param username
 * @param password
 */
public Driver(String username, String password) {
	this.username = username;
	this.password = password;
}

public void setAssigned(List<WorkSchedule> assigned) {
	this.assigned = assigned;
}

/**
 * Check Password method checks inputed 
 * @author Jack
 */
public boolean checkPassword(String password) {
	boolean flag = false;
	if (this.password.equals(password)) {
		flag = true;
	}
	return flag;
}
/**
 * Checks the availability of the driver
 * @param aDate input parameter to check the data that  the driver is available
 * @return whether the driver is available or not in a boolean 
 */
public boolean isAvaliable(Date sDate, Date eDate) {

	Date dayBefore = sDate;
	
	Calendar calendar = Calendar.getInstance();
    dayBefore = calendar.getTime();

    calendar.add(Calendar.DAY_OF_YEAR, -1);
    for(WorkSchedule currWS : assigned) {
	if(eDate.after(currWS.getEndDate()) || dayBefore.before(currWS.getStartDate())) {
		return true;
	}else {
		return false;
	}
    }
	return true;
}

/**
 * Creates a new Schedule 
 * @param newSchedule Scehdule to be assigned to the driver
 */
public  void setSchedule(WorkSchedule newSchedule) {
	this.assigned.add(newSchedule); 
}

public List<WorkSchedule> getAssigned() {
	return assigned ;
}

public String getUsername() {
	return username;
}

public void setUsername(String username) {
	this.username = username;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}

/* Storing & Reading Data to/from file
 
public static void writeToFile(Driver tempDriver){
		
	try {
	FileOutputStream fos = new FileOutputStream("driverData.ser", true);
    ObjectOutputStream oos = new ObjectOutputStream(fos);
    
    oos.writeObject(tempDriver);
    
    oos.close();
    fos.close();
	}
	catch (IOException ioe) 
    {
        ioe.printStackTrace();
    }
}

@SuppressWarnings("finally")
public static ArrayList<Driver> readFromFile(){
	
	ArrayList<Driver> temp = new ArrayList<>();
	try
    {
		
        FileInputStream fis = new FileInputStream("driverData.ser");
        ObjectInputStream ois = = new ObjectInputStream(fis);;

        

        ois.close();
        fis.close();
    } 
    catch (IOException ioe) 
    {
        ioe.printStackTrace();
    } 
	finally 
	{
		return temp;
	}
}
*/

}
