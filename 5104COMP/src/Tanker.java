/**
 * tanker is a type of vehicle for liquid transport
 * @author Pablo Prince
 *
 */
public class Tanker extends Vehicle {
	public int liquidCapacity;
	public String liquidtype;
	/**
	 * Constructor
	 * @param make
	 * @param model
	 * @param weight
	 * @param regNo
	 * @param liquidCapacity
	 * @param liquidtype
	 */
	public Tanker(String make, String model, int weight, String regNo, int liquidCapacity,String liquidtype) {
		super(make, model, weight, regNo);
		this.liquidCapacity = liquidCapacity;
		this.liquidtype = liquidtype;
		// TODO Auto-generated constructor stub
	}
	public int getLiquidCapacity() {
		return liquidCapacity;
	}
	public void setLiquidCapacity(int liquidCapacity) {
		this.liquidCapacity = liquidCapacity;
	}
	public String getLiquidtype() {
		return liquidtype;
	}
	public void setLiquidtype(String liquidtype) {
		this.liquidtype = liquidtype;
	}

}
